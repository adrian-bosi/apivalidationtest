Feature: API data validation feature
  As an API owner I want to retrieve data from an API so that I can validate that the values are correct.

  Scenario: Validate data of a specific Category
    Given I call the category ID 6327
    When I retrieve the data from the API
    Then the name should be Carbon credits
    And CanRelist should be True
    And the Gallery promotion's description should contain 2x larger image
