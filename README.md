# API Validation Test for Assurity

## The following project validates the following requirements:

###API under test:
```
https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false
```

###Acceptance Criteria:
```
* Name = "Carbon credits"
* CanRelist = true
* The Promotions element with Name = "Gallery" has a Description that contains the text "2x larger image"
```

###Stack used:
* Java 1.8
* Maven 3.5.4
* Jersey for HTTP GET calls
* Jackson for Json parsing
* JUnit for test development
* Cucumber for features specification

###How to run:
* git clone https://adrian-bosi@bitbucket.org/adrian-bosi/apivalidationtest.git
* Navigate to project folder
* mvn clean test

###Structure
   
    .
    ├── src
    │   └── test
    │       ├── java              
    │       │    └── APIValidationTest              #All JAVA classes and code development
    │       │         ├── ApiClient                 #API Client implementation      
    │       │         ├── Model                     #Contains mapping for json
    │       │         └── APIValidationTestSteps    #Steps definition for all features
    │       │
    │       └── resources
    │            └── APIValidationTest              #Cucumber features definition
    ├── pom.xml
    └── README.md
    
     

###Author

Adrian Bosi