package APIValidationTest.ApiClient;

import APIValidationTest.Model.Category;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class ApiClient {

    private static final String url = "https://api.tmsandbox.co.nz/v1/Categories/%s/Details.json?catalogue=false";

    public static Category getCategory (int categoryID){

        String urlToCall = String.format(url, categoryID);

        Client client = ClientBuilder.newClient().register(JacksonJsonProvider.class);

        Response response = client.target(urlToCall).request().get();

        Category result = response.readEntity(Category.class);

        return result;


    }
}
