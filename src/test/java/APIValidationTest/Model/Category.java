package APIValidationTest.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Category {

    public String Name;
    public Boolean CanRelist;
    public List<Promotion> Promotions;

}
