package APIValidationTest;

import APIValidationTest.ApiClient.ApiClient;
import APIValidationTest.Model.Category;
import APIValidationTest.Model.Promotion;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class APIValidationTestSteps {

    private int categoryID;
    private Category result;

    @Given("^I call the category ID (.*)$")
    public void call_category_ID(int cucumberCategoryID) {
        categoryID = cucumberCategoryID;
    }

    @When("^I retrieve the data from the API$")
    public void call_API() {
        result = ApiClient.getCategory(categoryID);
        assertNotNull("Category from api is null", result);
    }

    @Then("^the name should be (.*)$")
    public void expected_name(String expectedName) {
        assertEquals(result.Name, expectedName);
    }

    @And("^CanRelist should be (.*)$")
    public void expected_CanRelist(String expectedCanRelist) {
        Boolean expectedRelist = Boolean.valueOf(expectedCanRelist);
        assertEquals(result.CanRelist, expectedRelist);
    }

    @And("^the (.*) promotion's description should contain (.*)$")
    public void expected_description_for_given_name(String expectedName, String expectedDescription) {
        int promotionsSize = result.Promotions.size();
        boolean foundPromotion = false;
        for (int i = 0; i < promotionsSize; i++) {
            Promotion promotionsName = result.Promotions.get(i);
            if (promotionsName.Name.equals(expectedName)) {
                foundPromotion = true;
                String selectedPromotionsNameDescription = promotionsName.Description;
                assertTrue(selectedPromotionsNameDescription.contains(expectedDescription));
            }

        }
        assertTrue("Promotion not found", foundPromotion);
    }
}




